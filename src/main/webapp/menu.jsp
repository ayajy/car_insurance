<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>车险管理系统</title>
    <%@include file="/common/common.jsp" %>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>

<div class="easyui-layout" fit="true">
    <div data-options="region:'north'" style="height:50px">
        <div style="background-color: #177bbb"><h1 align="center">车险管理系统</h1></div>

    </div>
    <div data-options="region:'south',split:true" style="height:50px;">
        <p align="center">盗版必究</p>
    </div>
    <div data-options="region:'west',split:true" style="width:150px;">
        <div class="easyui-accordion" style="width:500px;height:300px; fit:true;">
            <div title="菜单" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:10px;">
                <ul id="tree"></ul>
            </div>
            <div title="公司简介" data-options="iconCls:'icon-help'" style="padding:10px;">
                <p>小码哥java</p>
            </div>
        </div>
    </div>
    <div data-options="region:'center',iconCls:'icon-ok'">
        <div id="tabs">
            <div title="首页"> 这是网站主页</div>
        </div>
    </div>

</div>
</body>
</html>