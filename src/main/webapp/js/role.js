$(function() {
	var role_datagrid = $("#role_datagrid");
	var role_form = $("#role_form");
	var role_dialog = $("#role_dialog");
	role_datagrid.datagrid({
		fitColumns : true,
		striped : true,
		url : '/role_list',
		fit : true,
		singleSelect : true,
		pagination : true,
		rownumbers : true,
		toolbar : '#toolbar',
		columns : [ [ {
			title : '角色编号',
			field : 'sn',
			width : 1
		},
			{
			title : '角色名称',
			field : 'name',
			width : 1
		}
		] ],
		onClickRow : function(rowIndex, rowData) {
			// 当前员工为离职状态,就禁用按钮
			if (!rowData.state) {
				$('#btn_remove').linkbutton('disable');
			} else {
				$('#btn_remove').linkbutton('enable');
			}
		}
	});

	role_dialog.dialog({
		width : 500,
		height : 500,
		buttons : '#btns',
		//closed : true
	});
	
	var allPermission = $("#allPermission");
	var selfPermission = $("#selfPermission");
	
	allPermission.datagrid({
		url:'/permission_list',
		rowColumns : true,
		title: '所有权限',
		height: 350,
		fitColumns: true,
		columns : [ [ {
			title : '权限名称',
			field : 'name',
			width : 1,
			align: 'center',
		}]],
		onDblClickRow:function(rowIndex,rowData){
			//先判断是否已经存在在已有权限中
			var rows=selfPermission.datagrid("getRows");
			for (var i = 0; i < rows.length; i++) {
				if (rowData.id == rows[i].id) {
					selfPermission.datagrid("unselectAll");
					//已经存在,选中一行数据
					selfPermission.datagrid("selectRow",i);
					return;
				}
			}
			//添加在已有权限中
			selfPermission.datagrid("appendRow",rowData);
		} 
	});
	
	selfPermission.datagrid({
		rowColumns : true,
		title: '已有权限',
		height: 350,
		fitColumns: true,
		columns : [ [ {
			title : '权限名称',
			field : 'name',
			width : 1,
			align: 'center',
		}]],
		onDblClickRow:function(rowIndex,rowData){
			//删除选中行
			selfPermission.datagrid("deleteRow",rowIndex);
		}
	});
	
	//方法统一管理 
	var methodObj = {
		save : function() {
			var url;
			// 如果有id就是更新
			if ($("[name='id']").val()) {
				url = "/role_update";
			} else {
				url = "/role_save";
			}
			role_form.form('submit', {
				url : url,
				onSubmit : function(param) {
					console.log(param);
					//获取已有权限中的所有结果数据
					var rows = selfPermission.datagrid("getRows");
					console.log(rows.length);
					for(var i = 0;i<rows.length;i++){
						param["permissions["+i+"].id"] = rows[i].id;
					}
				},
				success : function(data) {
					data = $.parseJSON(data);
					if (data.success) {
						$.messager.alert('温馨提示', data.msg, 'info', function() {
							// 关闭弹出框
							role_dialog.dialog('close');
							// 重新加载数据表格
							role_datagrid.datagrid("reload");
						});
					} else {
						$.messager.alert('温馨提示', data.msg);
					}
				}
			});

		},

		cancel : function() {
			role_dialog.dialog('close');
		},

		add : function() {
			// 清空表单
			role_form.form("clear");
			role_dialog.dialog('open');
			role_dialog.dialog('setTitle', '新增员工');
		},

		edit : function() {

			// 判断是否选中一条数据
			var row = role_datagrid.datagrid("getSelected");
			if (!row) {
				$.messager.alert('温馨提示', '请选中要编辑的数据', 'info');
				return;
			}
			// 清空表单
			role_form.form("clear");

			// 处理特殊属性
			if (row.dept) {
				row["dept.id"] = row.dept.id;
			}

			// 回显表单
			role_form.form("load", row);

			role_dialog.dialog('open');
			role_dialog.dialog('setTitle', '编辑员工');
		},

		remove : function() {
			// 判断是否选中一条数据
			var row = role_datagrid.datagrid("getSelected");
			if (!row) {
				$.messager.alert('温馨提示', '请选中要编辑的数据', 'info');
				return;
			}

			$.messager.confirm('确认', '您确认想要修改该员工的状态吗？', function(yes) {
				if (yes) {
					// 发送请求去修改员工的状态
					$.get("/roleloyee_updateState?id=" + row.id, function(data) {
						if (data.success) {
							$.messager.alert('温馨提示', data.msg, 'info',
									function() {
										// 重新加载数据表格
										role_datagrid.datagrid("reload");
									});
						} else {
							$.message.alert('温馨提示', data.msg);
						}
					})
				}
			});
		},

		searchForm : function() {
			var keyword = $("[name = 'keyword']").val();
			// 重新加载数据表格并且带上自定义参数
			role_datagrid.datagrid('load', {
				keyword : keyword,
			});
		}
	}
	//统一给按钮绑定事件
	$("[data-cmd]").on("click",function(){
		var methodName = $(this).data("cmd");
		methodObj[methodName]();
	})
});
