$(function() {
	var empDatagrid = $("#emp_datagrid");
	var empForm = $("#emp_form");
	var empDialog = $("#emp_dialog");
	empDatagrid.datagrid({
		fitColumns : true,
		striped : true,
		url : '/employee_list',
		fit : true,
		singleSelect : true,
		pagination : true,
		rownumbers : true,
		toolbar : '#toolbar',
		columns : [ [ {
			title : '用户名',
			field : 'username',
			width : 1
		}, {
			title : '真实名字',
			field : 'realname',
			width : 1
		}, {
			title : '电话',
			field : 'tel',
			width : 1
		}, {
			title : '邮箱',
			field : 'email',
			width : 1
		}, {
			title : '入职时间',
			field : 'inputtime',
			width : 1
		}, {
			title : '状态',
			field : 'state',
			width : 1,
			formatter : stateFormatter
		}, {
			title : '是否管理员',
			field : 'admin',
			width : 1,
			formatter : adminFormatter
		}, {
			title : '部门',
			field : 'dept',
			width : 1,
			formatter : deptFormatter
		} ] ],
		onClickRow : function(rowIndex, rowData) {
			// 当前员工为离职状态,就禁用按钮
			if (!rowData.state) {
				$('#btn_remove').linkbutton('disable');
			} else {
				$('#btn_remove').linkbutton('enable');
			}
		}
	});

	// 部门数据格式化
	function deptFormatter(value, row, index) {
		return value ? value.name : "";
	}

	// 状态数据格式化
	function stateFormatter(value, row, index) {
		return value ? "在职" : "<font color='red'>离职</font>";
	}

	// 是否管理员数据格式化
	function adminFormatter(value, row, index) {
		return value ? "是" : "否";
	}

	emp_dialog.dialog({
		width : 300,
		height : 300,
		buttons : '#btns',
		closed : true
	});


	var methodObj = {
		save : function() {
			var url;
			// 如果有id就是更新
			if ($("[name='id']").val()) {
				url = "/employee_update";
			} else {
				url = "/employee_save";
			}
			emp_form.form('submit', {
				url : url,
				onSubmit : function() {
				},
				success : function(data) {
					data = $.parseJSON(data);
					if (data.success) {
						$.messager.alert('温馨提示', data.msg, 'info', function() {
							// 关闭弹出框
							emp_dialog.dialog('close');
							// 重新加载数据表格
							emp_datagrid.datagrid("reload");
						});
					} else {
						$.messager.alert('温馨提示', data.msg);
					}
				}
			});

		},

		cancel : function() {
			emp_dialog.dialog('close');
		},

		add : function() {
			// 清空表单
			emp_form.form("clear");
			emp_dialog.dialog('open');
			emp_dialog.dialog('setTitle', '新增员工');
		},

		edit : function() {

			// 判断是否选中一条数据
			var row = emp_datagrid.datagrid("getSelected");
			if (!row) {
				$.messager.alert('温馨提示', '请选中要编辑的数据', 'info');
				return;
			}
			// 清空表单
			emp_form.form("clear");

			// 处理特殊属性
			if (row.dept) {
				row["dept.id"] = row.dept.id;
			}

			// 回显表单
			emp_form.form("load", row);

			emp_dialog.dialog('open');
			emp_dialog.dialog('setTitle', '编辑员工');
		},

		remove : function() {
			// 判断是否选中一条数据
			var row = emp_datagrid.datagrid("getSelected");
			if (!row) {
				$.messager.alert('温馨提示', '请选中要编辑的数据', 'info');
				return;
			}

			$.messager.confirm('确认', '您确认想要修改该员工的状态吗？', function(yes) {
				if (yes) {
					// 发送请求去修改员工的状态
					$.get("/employee_updateState?id=" + row.id, function(data) {
						if (data.success) {
							$.messager.alert('温馨提示', data.msg, 'info',
									function() {
										// 重新加载数据表格
										emp_datagrid.datagrid("reload");
									});
						} else {
							$.message.alert('温馨提示', data.msg);
						}
					})
				}
			});
		},

		searchForm : function() {
			var keyword = $("[name = 'keyword']").val();
			// 重新加载数据表格并且带上自定义参数
			emp_datagrid.datagrid('load', {
				keyword : keyword,
			});
		}
	}
	//统一给按钮绑定事件
	$("[data-cmd]").on("click",function(){
		var methodName = $(this).data("cmd");
		methodObj[methodName]();
	})
});
