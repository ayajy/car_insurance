$(function(){
	var tabsObj = $("#tabs");
	
	$("#tree").tree({
		url:'/data/tree_data.json',
		onClick:function(node){
			console.log(node)
			//判断选项卡是否已经存在
			if(tabsObj.tabs("exists",node.text)){
				//选中该选项卡
				tabsObj.tabs("select",node.text);
			}else{
				//新增一个选项卡
				tabsObj.tabs("add",{
					title: node.text,
					//只能引入页面中body部分
					//href: node.attributes.url,
					//通过iframe可以引入另一个文档的所有内容
					content:"<iframe src='"+node.attributes.url+"' style='width:100%;height:100%' frameborder=0 ></iframe>",
					closable:true
				});
			}
		}
	});
	tabsObj.tabs({
		fit:true
	});
	
});