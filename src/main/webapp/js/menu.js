﻿$(function(){
		var tabsObj = $("#tabs");
		tabsObj.tabs({
			fit:true
		})
	$("#tree").tree({
		url:'/data/tree_data.json',
		onClick:function(node){
			//判断选项卡是否存在
			if (tabsObj.tabs("exists",node.text)) {
				//存在的话就选中该选项卡
				tabsObj.tabs("selected",node.text);
			}else{
				//通过数据库获取的attributes是一个字符串需要转换成一个json对象
					node.attributes = $.parseJSON(node.attributes);
				tabsObj.tabs("add",{
					closable:true,
					title:node.text,
					/*href:node.attributes.url*/
					//通过iframe可以引入另一个文档的所有内容
					content:"<iframe src='"+node.attributes.url+"' style='width:100%;height:100%' frameborder=0></iframe>",
					closable:true
				});
			}
		}
	});
});

