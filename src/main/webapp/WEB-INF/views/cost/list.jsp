<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="1">
    <tr>
        <td>id</td>
        <td>手续费(佣金)比率</td>
        <td>业务员绩效工资比率</td>
        <td>团队主管业务绩效工资比率</td>
        <td>业务系列人员基本工资及福利比率</td>
        <td>团队销售管理费用比率</td>
        <td>营销服务部管理费用比率</td>
        <td>支公司销售管理费用比例</td>
        <td>中心支公司销售管理费用比例</td>
        <td>分公司销售管理费用比例</td>
    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.factorage}</td>
        <td>${item.performance}</td>
        <td>${item.teamPerformance}</td>
        <td>${item.basePayWelfare}</td>
        <td>${item.teamManage}</td>
        <td>${item.serviceManage}</td>
        <td>${item.branchManage}</td>
        <td>${item.centreBranchManage}</td>
        <td>${item.subManage}</td>
    </tr>
    </c:forEach>

<div>
    <a href="javascript:changePage(${current-1})">上一页</a>
    <c:forEach begin="0" end="${totalCount}" varStatus="status">
        <a href="javascript:changePage(${status.count})">${status.count}</a>
    </c:forEach>
    <a href="javascript:changePage(${current+1})">下一页</a>
</div>

</table>