<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<table border="1">
    <tr>
        <td>id</td>
        <td>机构部门</td>
        <td>产品</td>
        <td>包含下级</td>
        <td>申请单号</td>
        <td>投保人名称</td>
        <td>投保申请日期</td>
        <td>投保终止日期</td>
        <td>车牌号码</td>
    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.acceptId}</td>
        <td>${item.productId}</td>
        <td>${item.isInclude}</td>
        <td>${item.applyNum}</td>
        <td>${item.clientEleName}</td>
        <td><fmt:formatDate value="${item.beginDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
        <td><fmt:formatDate value="${item.endDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
        <td>${item.plateNum}</td>
    </tr>
    </c:forEach>
    <div>
        <a href="javascript:changePage(${current-1})">上一页</a>
        <c:forEach begin="1" end="${totalCount}" varStatus="status">
            <a href="javascript:changePage(${status.index})">${status.index}</a>
        </c:forEach>
        <a href="javascript:changePage(${current+1})">下一页</a>
    </div>


</table>