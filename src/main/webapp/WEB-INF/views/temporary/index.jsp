<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
    var _acceptId=$("input[name='acceptId']").val();
    var _productId=$("input[name='productId']").val();
    var _applyNum=$("input[name='applyNum']").val();
    var _clientEleName=$("input[name='clientEleName']").val();
    var _beginDate=$("input[name='beginDate']").val();
    var _endDate=$("input[name='endDate']").val();
    var _plateNum=$("input[name='plateNum']").val();
	function changePage(current) {
        $.ajax({
            method:"POST",
            url:"/temporary/list/view",
			data:{current:current,acceptId:_acceptId,productId:_productId,applyNum:_applyNum,
                clientEleName:_clientEleName,beginDate:_beginDate,endDate:_endDate,plateNum:_plateNum},
            success:function(response){

                $("#temporaryList").html(response);
            }})
    }
$(function(){
	changePage(1);

	
})

</script>
</head>
<body>
	机构部门:<input name="acceptId"><br>
	产品:<input name="productId"><br>
	申请单号: <input type="text" name="applyNum"><br>
	投保人名称: <input type="text" name="clientEleName"><br>
	投保申请日期:<input name="beginDate"><br>
	投保终止日期:<input name="endDate"><br>
	车牌号码:<input name="plateNum"><br>
	<button onclick="changePage(1)">query</button>
	<div id="temporaryList">

	</div>
</body>
</html>