<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
	function changePage(current) {
        var _exemptFloat=$("input[name='exemptFloat']").val();
        var _roadAccident=$("input[name='roadAccident']").val();
        var _unlawfulAct=$("input[name='unlawfulAct']").val();
        var _drinkAct=$("input[name='drinkAct']").val();
        var _ebrietyAct=$("input[name='ebrietyAct']").val();
        $.ajax({
            method:"POST",
            url:"/coefficient/list/view",
			data:{current:current,exemptFloat:_exemptFloat,roadAccident:_roadAccident,
			unlawfulAct:_unlawfulAct,drinkAct:_drinkAct,ebrietyAct:_ebrietyAct
           },
            success:function(response){
                $("#coefficientList").html(response);
            }})
    }
$(function(){
	changePage(1);
})

</script>
</head>
<body>
免浮动类型:<input name="exemptFloat"><br>
交通事故情况:<input name="roadAccident"><br>
交通违法行为: <input type="text" name="unlawfulAct"><br>
饮酒违法次数: <input type="text" name="drinkAct"><br>
醉酒违法行为:<input type="text" name="ebrietyAct"><br>
	<button onclick="changePage(1)">query</button>
	<div id="coefficientList">
	</div>
</body>
</html>