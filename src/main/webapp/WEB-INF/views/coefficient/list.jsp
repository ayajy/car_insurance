<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

date:<fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss"/>
<br>
<table border="1">
    <tr>
        <td>id</td>
        <td>免浮动类型</td>
        <td>交通事故情况</td>
        <td>交通违法行为</td>
        <td>饮酒违法次数</td>
        <td>醉酒违法行为</td>

    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.exemptFloat}</td>
        <td>${item.roadAccident}</td>
        <td>${item.unlawfulAct}</td>
        <td>${item.drinkAct}</td>
        <td>${item.ebrietyAct}</td>

    </tr>
    </c:forEach>
    <div>
        <a href="javascript:changePage(${current-1})">上一页</a>
        <c:forEach begin="1" end="${totalCount}" varStatus="status">
            <a href="javascript:changePage(${status.index})">${status.index}</a>
        </c:forEach>
        <a href="javascript:changePage(${current+1})">下一页</a>
    </div>

</table>