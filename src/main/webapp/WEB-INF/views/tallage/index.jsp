<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
	function changePage(current) {
        $.ajax({
            method:"POST",
            url:"/tallage/list/view",
			data:{current:current},
            success:function(response){

                $("#tallageList").html(response);
            }})
    }
$(function(){
	changePage(1);

	
})

</script>
</head>
<body>
	<div id="tallageList">

	</div>
</body>
</html>