<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="1">
    <tr>
        <td>id</td>
        <td>缴税方式</td>
        <td>缴税标志</td>
        <td>当前应缴类型</td>
        <td>纳税人识别号</td>
        <td>缴税车辆类型</td>
        <td>整备质量</td>
        <td>前次纳税年度</td>
        <td>前次交强险止日</td>
        <td>应缴税月份数</td>
        <td>减免税原因</td>
        <td>减免税证明号</td>
        <td>完税凭证号</td>
        <td>当年应缴</td>
        <td>往年补缴</td>
        <td>滞纳金</td>
    </tr>
    <c:forEach items="${list}" var="item">
        <tr>
            <td>${item.id}</td>
            <td>${item.way}</td>
            <td>${item.sign}</td>
            <td>${item.tType}</td>
            <td>${item.idenNumber}</td>
            <td>${item.cType}</td>
            <td>${item.cuWeight}</td>
            <td>${item.lastYear}</td>
            <td>${item.lastInsure}</td>
            <td>${item.tMonth}</td>
            <td>${item.reduceCause}</td>
            <td>${item.pTNum}</td>
            <td>${item.currentTax}</td>
            <td>${item.backTax}</td>
            <td>${item.overdueFine}</td>
        </tr>
    </c:forEach>
    <div>
        <a href="javascript:changePage(${current-1})">上一页</a>
        <c:forEach begin="0" end="${totalCount}" varStatus="status">
            <a href="javascript:changePage(${status.count})">${status.count}</a>
        </c:forEach>
        <a href="javascript:changePage(${current+1})">下一页</a>
    </div>
</table>