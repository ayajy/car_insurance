<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
	$(function(){
		$.ajax({
			method:"POST",
			url:"/client_selectAll",
			success:function(response){
				var htmlContent = "<table border='1'>"
				htmlContent +="<tr>";
				htmlContent +="<td>id</td>";
				htmlContent +="<td>客户姓名</td>";
				htmlContent +="<td>证件类型</td>";
				htmlContent +="<td>证件号码</td>";
				htmlContent +="<td>通讯地址</td>";
				htmlContent +="<td>证件号码</td>";
				htmlContent +="<td>邮编</td>";
                htmlContent +="<td>客户类型</td>";
                htmlContent +="<td>个人法人性质</td>";


				htmlContent +="</tr>";
				for(var index=0;index<response.length;index++){
					var item = response[index];
					htmlContent +=
						"<tr>"+
						"<td>"+item.id+"</td>"+
						"<td>"+item.name+"</td>"+
						"<td>"+item.idCard+"</td>"+
						"<td>"+item.idNumber+"</td>"+
						"<td>"+item.address+"</td>"+
						"<td>"+item.postCode+"</td>"+
						"<td>"+item.cType+"</td>"+
                        "<td>"+item.lenPerson+"</td>"+
                        "<td>"+item.hierarchy+"</td>"+
						"</tr>";

				}
				htmlContent +="</table>";
				$("#clientList").html(htmlContent);
			}})


	})

</script>
</head>
<body>
	<div id="clientList"></div>
</body>
</html>