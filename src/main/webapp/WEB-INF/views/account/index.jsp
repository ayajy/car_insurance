<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
	function changePage(current){
	    var _name=$("input[name='name']").val();
	    var _username=$("input[name='username']").val();
	    var _province=$("input[name='province']").val();
	    var _city=$("input[name='city']").val();
	    var _bank=$("input[name='bank']").val();
	    var _subBank=$("input[name='subBank']").val();
        $.ajax({
            method:"POST",
            url:"/account/list/view",
			data:{current:current,name:_name,username:_username,province:_province,city:_city,bank:_bank,subBank:_subBank},
            success:function(response){
                $("#accountList").html(response);
            }})
	}
$(function(){

	changePage(1);
	
})

</script>
</head>
<body>
	name:<input name="name"><br>
	username:<input name="username"><br>
	province: <input type="text" name="province"><br>
	city: <input type="text" name="city"><br>
	bank:<input name="bank"><br>
	subBank:<input name="subBank">
	<button onclick="changePage(1)">query</button>
	<div id="accountList">


	</div>
</body>
</html>