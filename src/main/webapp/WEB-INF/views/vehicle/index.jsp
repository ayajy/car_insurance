<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
    var _vehicleModel=$("input[name='vehicleModel']").val();
    var _name=$("input[name='name']").val();
    var _classify=$("input[name='classify']").val();
    var _busload=$("input[name='busload']").val();
    var _carrying=$("input[name='carrying']").val();
    var _carPrice=$("input[name='carPrice']").val();
	function changePage(current) {
        $.ajax({
            method:"POST",
            url:"/vehicle/list/view",
			data:{current:current,vehicleModel:_vehicleModel,name:_name,classify:_classify,busload:_busload
            ,carrying:_carrying,carPrice:_carPrice},
            success:function(response){

                $("#vehicleList").html(response);
            }})
    }
$(function(){


	changePage(1);
})

</script>
</head>
<body>
车型编码:<input name="vehicleModel"><br>
车型名称:<input name="name"><br>
车型分类: <input type="text" name="classify"><br>
核定载客(人): <input type="text" name="busload"><br>
额定载质量(千克):<input name="carrying"><br>
新车购置价:<input name="carPrice">
	<button onclick="changePage(1)">query</button>
	<div id="vehicleList">

	</div>
</body>
</html>