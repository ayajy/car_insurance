<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="1">
    <tr>
        <td>id</td>
        <td>车型编码</td>
        <td>车型名称</td>
        <td>车型分类</td>
        <td>核定载客(人)</td>
        <td>排气量</td>
        <td>额定载质量(千克)</td>
        <td>新车购置价</td>
    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.vehicleModel}</td>
        <td>${item.name}</td>
        <td>${item.classify}</td>
        <td>${item.busload}</td>
        <td>${item.exhaust}</td>
        <td>${item.carrying}</td>
        <td>${item.carPrice}</td>
    </tr>
    </c:forEach>

    <div>
        <a href="javascript:changePage(${current-1})">上一页</a>
        <c:forEach begin="1" end="${totalCount}" varStatus="status">
            <a href="javascript:changePage(${status.index})">${status.index}</a>
        </c:forEach>
        <a href="javascript:changePage(${current+1})">下一页</a>
    </div>
</table>