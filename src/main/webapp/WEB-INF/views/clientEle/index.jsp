<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
	function changePage(current) {
        var _name=$("input[name='name']").val();
        var _nature=$("input[name='nature']").val();
        var _antiLaunGrade=$("input[name='antiLaunGrade']").val();
        var _papersType=$("input[name='papersType']").val();
        var _papersNum=$("input[name='papersNum']").val();
        var _address=$("input[name='address']").val();
        var _aReplenish=$("input[name='aReplenish']").val();
        var _tel=$("input[name='tel']").val();
        $.ajax({
            method:"POST",
            url:"/clientEle/list/view",
			data:{current:current,name:_name,nature:_nature,antiLaunGrade:_antiLaunGrade,
                papersType:_papersType,papersNum:_papersNum,address:_address,
                aReplenish:_aReplenish,tel:_tel},
            success:function(response){
                $("#clientEleList").html(response);
            }})
    }
$(function(){
	changePage(1);
})

</script>
</head>
<body>
	name:<input name="name"><br>
	nature:<input name="nature"><br>
	antiLaunGrade: <input type="text" name="antiLaunGrade"><br>
	papersType: <input type="text" name="papersType"><br>
	papersNum:<input name="papersNum"><br>
	address:<input name="address"><br>
	aReplenish:<input name="aReplenish"><br>
	tel:<input name="tel">
	<button onclick="changePage(1)">query</button>
	<div id="clientEleList">

	</div>
</body>
</html>