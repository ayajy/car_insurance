<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="1">
    <tr>
        <td>id</td>
        <td>名称</td>
        <td>个人/法人</td>
        <td>反洗钱风险等级</td>
        <td>证件类型</td>
        <td>证件号码</td>
        <td>通讯地址</td>
        <td>地址补充</td>
        <td>电话</td>
    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.nature}</td>
        <td>${item.antiLaunGrade}</td>
        <td>${item.papersType}</td>
        <td>${item.papersNum}</td>
        <td>${item.address}</td>
        <td>${item.aReplenish}</td>
        <td>${item.tel}</td>
    </tr>
    </c:forEach>

    <div>
        <a href="javascript:changePage(${current-1})">上一页</a>
        <c:forEach begin="1" end="${totalCount}" varStatus="status">
        <a href="javascript:changePage(${status.index})">${status.index}</a>
        </c:forEach>
        <a href="javascript:changePage(${current+1})">下一页</a>
    </div>
</table>