<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="1">
    <tr>
        <td>id</td>
        <td>名字</td>
        <td>用户名</td>
        <td>银行省</td>
        <td>银行市</td>
        <td>银行名称</td>
        <td>支行名称</td>
    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.username}</td>
        <td>${item.province}</td>
        <td>${item.city}</td>
        <td>${item.bank}</td>
        <td>${item.subBank}</td>
    </tr>
    </c:forEach>

<div>
    <a href="javascript:changePage(${current-1})">上一页</a>
    <c:forEach begin="1" end="${totalCount}"  varStatus="status">
        <a href="javascript:changePage(${status.index})">${status.index}</a>
    </c:forEach>
    <a href="javascript:changePage(${current+1})">下一页</a>
</div>

</table>