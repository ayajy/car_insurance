<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
$(function() {
    $.ajax({
        method: "POST",
        url: "/car_selectAll",
        success: function (response) {
            var htmlContent = "<table border='1'>"
            htmlContent += "<tr>";
            htmlContent += "<td>id</td>";
            htmlContent += "<td>是否上牌</td>";
            htmlContent += "<td>品牌</td>";
            htmlContent += "<td>车辆种类</td>";
            htmlContent += "<td>新车购置价</td>";
            htmlContent += "<td>车乘号</td>";
            htmlContent += "<td>发动机号</td>";
            htmlContent += "<td>核定制质量</td>";
            htmlContent += "<td>排气量</td>";
            htmlContent += "<td>号牌种类</td>";
            htmlContent += "<td>是否进口</td>";
            htmlContent += "<td>车牌号码</td>";
            htmlContent += "<td>是否新车</td>";
            htmlContent += "<td>是否外地</td>";
            htmlContent += "<td>初登日期</td>";
            htmlContent += "<td>功率</td>";
            htmlContent += "<td>转移登记(过户)日期</td>";
            htmlContent += "<td>使用性质</td>";
            htmlContent += "</tr>";
            for (var index = 0; index < response.length; index++) {
                var item = response[index];
                htmlContent +=
                    "<tr>" +
                    "<td>" + item.id + "</td>" +
                    "<td>" + item.apply + "</td>" +
                    "<td>" + item.brand + "</td>" +
                    "<td>" + item.carKind + "</td>" +
                    "<td>" + item.carPrice + "</td>" +
                    "<td>" + item.carriageNum + "</td>" +
                    "<td>" + item.customQua + "</td>"+
                    "<td>" + item.engineNum + "</td>" +
                    "<td>" + item.exhaust + "</td>" +
                    "<td>" + item.flapperKind + "</td>" +
                    "<td>" + item.importCar + "</td>" +
                    "<td>" + item.licenseNum + "</td>" +
                    "<td>" + item.newCar + "</td>" +
                    "<td>" + item.nonlocal + "</td>" +
                    "<td>" + item.registerDate + "</td>" +
                    "<td>" + item.power + "</td>" +
                    "<td>" + item.shiftDate + "</td>" +
                    "<td>" + item.useNature + "</td>"+
                    "</tr>";

            }
            htmlContent += "</table>";
            $("#carList").html(htmlContent);
        }
    })


})

    </script>
</head>
<body>

<div id="carList">

</div>
</body>
</html>