<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
$(function(){
	$.ajax({
		method:"POST",
		url:"/insure_selectAll",
		success:function(response){
			var htmlContent = "<table border='1'>"
			htmlContent +="<tr>";
			htmlContent +="<td>id</td>";
			htmlContent +="<td>产品</td>";
			htmlContent +="<td>联合投保</td>";
			htmlContent +="<td>模板类型</td>";
			htmlContent +="<td>车牌号</td>";
			htmlContent +="<td>承保机构</td>";
			htmlContent +="</tr>";
			for(var index=0;index<response.length;index++){
				var item = response[index];
				htmlContent +=
					"<tr>"+
					"<td>"+item.id+"</td>"+
					"<td>"+item.productId+"</td>"+
					"<td>"+item.uniteIns+"</td>"+
					"<td>"+item.modelId+"</td>"+
					"<td>"+item.plateNum+"</td>"+
					"<td>"+item.acceptId+"</td>"+
					"</tr>";
			}
			htmlContent +="</table>";
			$("#insureList").html(htmlContent);
		}})
})
</script>
</head>
<body>
	<div id="insureList">

	</div>
</body>
</html>