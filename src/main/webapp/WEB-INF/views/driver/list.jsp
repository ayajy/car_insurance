<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<table border="1">
    <tr>
        <td>id</td>
        <td>驾驶员名称</td>
        <td>性别</td>
        <td>驾驶证类型</td>
        <td>驾驶证号码</td>
        <td>职业类别</td>
        <td>出生日期</td>
        <td>初次领证</td>
    </tr>
    <c:forEach items="${list}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.sex}</td>
        <td>${item.drivingType}</td>
        <td>${item.drivingNum}</td>
        <td>${item.occupation}</td>
        <td><fmt:formatDate value="${item.birthDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
        <td><fmt:formatDate value="${item.birthDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
    </tr>
    </c:forEach>

    <div>
        <a href="javascript:changePage(${current-1})">上一页</a>
        <c:forEach begin="1" end="${totalCount}" varStatus="status">
            <a href="javascript:changePage(${status.index})">${status.index}</a>
        </c:forEach>
        <a href="javascript:changePage(${current+1})">下一页</a>
    </div>
</table>