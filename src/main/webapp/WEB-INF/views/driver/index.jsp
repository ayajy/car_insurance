<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/common/common.jsp" %>
<script type="text/javascript">
	function changePage(current) {
        var _name=$("input[name='name']").val();
        var _sex=$("input[name='sex']").val();
        var _drivingType=$("input[name='drivingType']").val();
        var _drivingNum=$("input[name='drivingNum']").val();
        var _occupation=$("input[name='occupation']").val();
        var _birthDate=$("input[name='birthDate']").val();
        var _firstDate=$("input[name='firstDate']").val();
        $.ajax({
            method:"POST",
            url:"/driver/list/view",
            data:{current:current,name:_name,
			sex:_sex,drivingType:_drivingType,drivingNum:_drivingNum,
			occupation:_occupation,birthDate:_birthDate,firstDate:_firstDate},
            success:function(response){

                $("#driverList").html(response);
            }})
    }
$(function(){
	changePage(1);

	
})

</script>
</head>
<body>
驾驶员名称:<input name="name"><br>
性别:<input name="sex"><br>
驾驶证类型: <input type="text" name="drivingType"><br>
驾驶证号码: <input type="text" name="drivingNum"><br>
职业类别:<input name="occupation"><br>
出生日期:<input name="birthDate"><br>
初次领证:<input name="firstDate">
	<button onclick="changePage(1)">query</button>
	<div id="driverList">

	</div>
</body>
</html>