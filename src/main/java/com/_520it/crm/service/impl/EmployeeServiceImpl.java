package com._520it.crm.service.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com._520it.crm.domain.Employee;
import com._520it.crm.mapper.EmployeeMapper;
import com._520it.crm.page.PageResult;
import com._520it.crm.query.EmployeeQuery;
import com._520it.crm.service.IEmployeeService;

@Service
public class EmployeeServiceImpl implements IEmployeeService{

	@Autowired
	private EmployeeMapper mapper;
	
	public int deleteByPrimaryKey(Long id) {
		return mapper.deleteByPrimaryKey(id);
	}

	public int insert(Employee record) {
		return mapper.insert(record);
	}

	public Employee selectByPrimaryKey(Long id) {
		return mapper.selectByPrimaryKey(id);
	}

	public List<Employee> selectAll() {
		return mapper.selectAll();
	}

	public int updateByPrimaryKey(Employee record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Employee getEmployeeByLogin(String username, String password) {
		return mapper.getEmployeeByLogin(username, password);
	}

	@Override
	public PageResult queryPageResult(EmployeeQuery qo) {
		Long count = mapper.queryPageCount(qo);
		if(count>0){
			//查询结果集
			return new PageResult(count, mapper.queryPageResult(qo));
		}
		return new PageResult(count,Collections.EMPTY_LIST);
	}

	@Override
	public void updateState(Long id) {
		mapper.updateState(id);
	}

	
	
	
	
	
	
	
	
	
}
