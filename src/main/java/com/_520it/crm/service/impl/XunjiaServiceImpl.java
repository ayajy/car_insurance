package com._520it.crm.service.impl;

import com._520it.crm.domain.Xunjia;
import com._520it.crm.mapper.XunjiaMapper;
import com._520it.crm.service.IXunjiaService;
import com.alibaba.druid.filter.AutoLoad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2018/3/13 0013.
 */
@Service
public class XunjiaServiceImpl implements IXunjiaService{



    @Autowired
    XunjiaMapper xunjiaMapper;

    @Override
    public List<Xunjia> findAll() {
        return xunjiaMapper.findAll();
    }

    @Override
    public void delete(Long id) {
        xunjiaMapper.delete(id);
    }
}
