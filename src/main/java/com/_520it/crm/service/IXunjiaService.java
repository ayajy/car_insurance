package com._520it.crm.service;

import com._520it.crm.domain.Xunjia;

import java.util.List;

/**
 * Created by Administrator on 2018/3/13 0013.
 */
public interface IXunjiaService {
    List<Xunjia> findAll();

    void delete(Long id);
}
