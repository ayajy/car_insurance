package com._520it.crm.mapper;

import com._520it.crm.domain.Employee;
import com._520it.crm.query.EmployeeQuery;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Employee record);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectAll();

    int updateByPrimaryKey(Employee record);
    
    Employee getEmployeeByLogin(@Param("username") String username, @Param("password") String password);
    
    Long queryPageCount(EmployeeQuery qo);
    
    List<Employee> queryPageResult(EmployeeQuery qo);
    
    void updateState(Long id);
}