package com._520it.crm.util;

public class JsonResult {
	private boolean success;
	private String msg;
	public static JsonResult createSuccess(){
		return new JsonResult(true,"创建成功");
	}
	public static JsonResult createError(){
		return new JsonResult(false,"创建失败");
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public JsonResult(boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}
	public JsonResult() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
