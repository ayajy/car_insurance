package com._520it.crm.web.controller;

import com._520it.crm.service.IXunjiaService;
import com._520it.crm.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("xunjia")
public class XunjiaController {

    @Autowired
    IXunjiaService xunjiaService;

    @RequestMapping("index")
    public String index(Model model){
        model.addAttribute("items",xunjiaService.findAll());
        return "xunjia/index";
    }

    @RequestMapping("delete")
    @ResponseBody
    public JsonResult delete(Long id){
        xunjiaService.delete(id);
        return JsonResult.createSuccess();
    }

}
