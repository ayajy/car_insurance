/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : crm

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2018-03-13 00:49:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `inputtime` datetime DEFAULT NULL,
  `state` date DEFAULT NULL,
  `admin` bit(1) DEFAULT NULL,
  `dept` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('1', '123', 'zhangsan', '123', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for xunjia
-- ----------------------------
DROP TABLE IF EXISTS `xunjia`;
CREATE TABLE `xunjia` (
  `id` bigint(20) NOT NULL,
  `danhao` varchar(255) DEFAULT NULL,
  `chanpin` varchar(255) DEFAULT NULL,
  `chepai` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xunjia
-- ----------------------------
